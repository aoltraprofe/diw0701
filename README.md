#README / LEEME

*Práctica:* 15

*Unidad:* UD05. jQuery

*Módulo:* Diseño de Interfaces WEB

*Ciclo Formativo:* Desarrollo de Aplicaciones WEB

*Objetivo:* Poner en práctica mecanismos propocionados por jQuery para la creación de interfaces.

##Author / Autor

Alfredo Oltra (mail: [alfredo.ptcf@gmail.com](alfredo.ptcf@gmail.com) / twitter: [@aoltra](https://twitter.com/aoltra))

##License / Licencia

Creative Commons: [by-no-sa](http://es.creativecommons.org/blog/licencias/) 

##Licencias imágenes / Images licenses

- [Icono frikilandia](https://visualpharm.com/free-icons/house%20stark-595b40b65ba036ed117d2e28) Linkware license 

- [Iconos](http://glyph.smarticons.co)  Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)

- [Europosters](https://www.europosters.es/)
