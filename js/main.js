var carritoShow = false;    // indica si el carrito está visible o no

$(function() {

    var cloneProductDrag;

    $("#btn-clear").css("display","none");
    $("#btn-buy").css("display","none");
    $("#btn-prev").css("display","none");
    $("#btn-next").css("display","none");

    // capturo el evento Bootstrap para conocer el estado del carrito
    $('#panel-carrito').on('shown.bs.collapse', function () {
        carritoShow = true;
     });

     $('#panel-carrito').on('hidden.bs.collapse', function () {
        carritoShow = false;
     });

	$(".producto").on("dblclick",function(event) {

        if (carritoShow == false) return;

		if (productStockUpdate($(this), -1) == true) {
            updateCartItemsNumber(1);
            updateCartTotalPrice(parseFloat($(this).find(".price").text().replace("€","")));

            addItemToCart($(this));
		}
    });
    
    $("#btn-clear").on("click",function(event) {
        $('#confirm-clear').modal("show");		
    });

    $("#yes-clear").on("click", function(event) {
        $('#confirm-clear').modal("hide");
        $(".remove-icon").trigger("click");
    });
    
    $("#btn-prev").on("click", function(event) {
		scrollsCartToLeft(50);
	});

	$("#btn-next").on("click", function(event) {
		scrollsCartToRight(50);
    });
    
    // en función del ancho de la ventana compruebo si hay o no
    // que mostrar los botones de desplazamiento
    $(window).on("resize",function(event) {
        showScrollButtons();
    });

    $(".producto").draggable ({
        helper : function() {
            tmpDrag = $("<div class='tmp-drag'></div>");
            tmpDrag.append($(this).children('.card-img-top').clone());
            return tmpDrag;
        },
        revert: false,
        zIndex: 10000,
        cursor: "auto",
        cursorAt: { top: 40, left: 50 },
        start: function (event, ui) {
            cloneProductDrag = $(this);
        }
    });

    // carrito es droppable
    $("#carrito-lista").droppable({
        accept : ".producto",
        drop: function (event, ui) {
            cloneProductDrag.trigger("dblclick");
            $(this).removeClass("drop-hover").removeClass("drop-active");    
        },
        over: function (event, ui) {
            $(this).addClass("drop-hover");
        },
        out: function (event, ui) {
            $(this).removeClass("drop-hover");
        },
        activate: function (event, ui) {
            $(this).addClass("drop-active");
            $("#boton-carrito").addClass("drop-active-button");
        },
        deactivate: function (event, ui) {
            $(this).removeClass("drop-active").removeClass("drop-hover");
            $("#boton-carrito").removeClass("drop-active-button");
        }
    });
});

/**
 * Actualiza el stock del producto
 * @param {jQuery Object} item elemento producto 
 * @param {integer} increase cantidad a incrementar en el stock
 */
function productStockUpdate(item, increase) {
    var result = false;
    var stockElement = item.find(".stock");
    var stock = parseInt(stockElement.text().replace(/^Unidad/,"")), newStock;

    if (stock + increase >= 0) {
        newStock = stock + increase;

        var htmlCode = stockElement.html();
       
        if (newStock == 1) 
            htmlCode = htmlCode.replace(stock + " Unidades", "1 Unidad");
        else {
            // Ojo! no cambiar el orden de (Unidades|Unidad), al ser un | se queda 
            // con el primero que le encaja
            htmlCode = htmlCode.replace(/[0-9]+\s(Unidades|Unidad)/, newStock + " Unidades");
        }
       
        stockElement.html(htmlCode);
        item.find(".ultimas-unidades").remove();
        item.find(".agotado").remove();
    
		if (newStock == 0)
			item.children(".card-body").append("<p class='card-text text-right agotado'><small class='text-danger'>Agotado</small></p>");
		else if (stock < 4)
            item.children(".card-body").append("<p class='card-text text-right ultimas-unidades'><small class='text-warning'>Últimas unidades</small></p>");
        
		result = true;
	}
	
	return result;
}

/**
 * Actualiza en el carro el número de items
 * @param {integer} increase número de items a actualizar
 */
function updateCartItemsNumber(increase) {
	var numItems = parseInt($("#num-item").val());
	numItems += increase;
	$("#num-item").val(numItems);
}

/**
 * Actualiza en el carro el precio total
 * @param {float} increase incremento en el precio total
 */
function updateCartTotalPrice(increase) {
	var totalPrice = parseFloat($("#precio-item").val());
	totalPrice += parseFloat(increase);
	$("#precio-item").val(totalPrice + " €");
}

/**
 * Añade un item al carrito
 * @param {jQuery object} product card del producto
 */
function addItemToCart(product) {
    // botón de borrado del item del carrito 
    var remove = $('<img src="iconos/si-glyph-delete.svg" class="remove-icon">');
    remove.on("click",function(event){
        removeItemFormCart($(this));
    });

    var id = "_" + product.attr("id");
    var image = product.children("img").clone();
    var title = product.find('h4').text();
    var price = product.find('.price').clone();
    
    // tooltip 
    price.children('.texto-inline').remove();
    var item= $("<div data-toggle='tooltip' data-placement='bottom' title='"+title+"'></div>")
            .addClass("cart-item")
            .attr("id", id)
            .append(image)
            .append(remove)
            .append(price);

    item.tooltip();
    item.on("mouseenter",function(event){
        $(this).children(".remove-icon").css("visibility","visible");
    });
    item.on("mouseleave",function(event){
        $(this).children(".remove-icon").css("visibility","hidden");
    });

    // hay que renderizarlo antes de poder tomar medidas
    $("#carrito-items").prepend(item);

    item.addClass("cart-item--animate-in");

    // para evitar que los item se coloquen en vertical, voy ampliando
    // el ancho del carrito pra que vayan cabiendo
    growCartWidth();

     /* si no hay items se ocultan los botones comprar
    y vaciar  */
    if ($("#carrito-items").children().length > 0) {
        $("#btn-clear").css("display","inline-block");
        $("#btn-buy").css("display","inline-block");
    }

    showScrollButtons();
}

/**
 * Borrar un item del carrito
 * @param {jQuery object} item item del carrito a borrar
 */
function removeItemFormCart(item) {
	var id = item.parent().attr("id");
    id = id.substring(1);
    
    // ancho con márgenes
    var width = item.parent().outerWidth(true);
    item.parent().tooltip("dispose");
    
	productStockUpdate($("#"+id), 1);
	
	updateCartItemsNumber(-1);
    updateCartTotalPrice(-parseFloat(item.siblings(".price").text().replace("€","")));
    
    item.parent().addClass("cart-item--animate-out");
    item.css("display","none");
    item.children(".price").remove();

    item.parent().on("animationend", function() {    
        item.parent().remove(); 
        // si no hay items se ocultan los botones comprar y vaciar 
        if ($("#carrito-items").children().length == 0) {
            $("#btn-clear").css("display","none");
            $("#btn-buy").css("display","none"); 
        }
        growCartWidth();

        showScrollButtons();
    });
}

/**
 * Amplia o reduce el ancho del carrito
 */
function growCartWidth() {

    var numCartItems = $("#carrito-items").children().length;
    var widthCart = $("#carrito-items").width();
    var paddingCart = $("#carrito-items").innerWidth() - $("#carrito-items").width();
    var widthList = $("#carrito-lista").width() - paddingCart;
   
    var widthNeeded = numCartItems * $(".cart-item").outerWidth(true) + paddingCart;
    console.log("num", numCartItems, "need", widthNeeded, "widthCart", widthCart, "widthList", widthList);
    if (widthNeeded >= widthCart) 
        $("#carrito-items").width(widthNeeded);
    else if (widthNeeded > widthList) {
        $("#carrito-items").width(widthNeeded);
    }
    else 
        $("#carrito-items").width(widthList);
}

/**
 * Desplaza el carrito hacia la derecha
 * @param {int} displ pixel a desplzar hacia la derecha
 */
function scrollsCartToRight(displ) {
    var pos = $("#carrito-items").offset();
    var cartRight = pos.left + $("#carrito-items").width();
    var containerRight = $("#carrito-lista").offset();
    var paddingCart = $("#carrito-items").innerWidth() - $("#carrito-items").width();
    // el borde y los márgenes son simétricos
    containerRight.left += $("#carrito-lista").outerWidth();

	if (cartRight >= containerRight.left) {
        if (cartRight - displ < containerRight.left)
            pos.left = containerRight.left - $("#carrito-items").width() - paddingCart;
        else
            pos.left -= displ;

        $("#carrito-items").offset(pos);
    }
}

/**
 * Desplaza el carrito hacia la izquierda
 * @param {int} displ pixel a desplzar hacia la izquierda
 */
function scrollsCartToLeft(displ) {
    var pos = $("#carrito-items").offset();
    
    var containerLeft = $("#carrito-lista").offset();
    // el borde y los márgenes son simétricos
	containerLeft.left += ($("#carrito-lista").outerWidth() - $("#carrito-lista").width())*.5;
 
	if (pos.left < containerLeft.left) {
        if (pos.left + displ > containerLeft.left)
            pos.left = containerLeft.left;
        else
            pos.left += displ;

        $("#carrito-items").offset(pos);
    }
}

function showScrollButtons() {

    var widthCart = $("#carrito-items").width();
    var widthList = $("#carrito-lista").width();

    if (widthCart > widthList) {
        $("#btn-prev").css("display","inline-block");
        $("#btn-next").css("display","inline-block"); 
    } else {
        $("#btn-prev").css("display","none");
        $("#btn-next").css("display","none"); 
    }
}